<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Страница для неавторизированных пользователей';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <h2>Страница для неавторизированных пользователей</h2>
            <p><a class="btn btn-default" href="<?= Url::to(['site/signup']) ?>">Регистрация</a></p>
            <p><a class="btn btn-default" href="<?= Url::to(['site/login']) ?>">Авторизация</a></p>

        </div>

    </div>
</div>
